package org.legite.trf;

import java.time.LocalDate;
import java.time.Period;
import java.time.temporal.ChronoUnit;


public class Trf {
    public static final String WHITE_B = "\033[1;37m";

    public static void main(String[] args) {
        LocalDate today = LocalDate.now();
        //LocalDate dateFormation = LocalDate.of(2023, 11, 20);
        //Period dateFormationDiff = Period.between(today, dateFormation);

        LocalDate dateRepriseAn = LocalDate.of(2024, 01, 3);
        Period dateRepriseAnDiff = Period.between(today, dateRepriseAn);


        // stage
        LocalDate dateStageDebut = LocalDate.of(2024, 02, 19);
        Period dateStageDebutDiff = Period.between(today, dateStageDebut);

        LocalDate dateStageFin = LocalDate.of(2024, 05, 8);
        Period dateStageFinDiff = Period.between(today, dateStageFin);

        // nbre de jours du stage
        Period period_stageDuree = Period.between(dateStageDebut, dateStageFin);
        long stageJoursNb=ChronoUnit.DAYS.between(dateStageDebut, dateStageFin);
        //Integer stageJoursNb = period_stageDuree.getDays();
        System.out.printf("%25s:%3d", "Nombre de jours de stage" ,stageJoursNb);
        System.out.println("");

        LocalDate dateExamen = LocalDate.of(2024, 07, 01);
        Period dateExamenDiff = Period.between(today, dateExamen);


        Period dateFormationRestant = Period.between(today, dateExamen.minusDays(stageJoursNb));

        // Console.inspectVar("diff.toString()", diff.toString());

        System.out.println(WHITE_B+"Temps restant avant: ");
        //System.out.println("reprise an dans:"      + dateRepriseAnDiff.getMonths()  + "mois " + dateRepriseAnDiff.getDays() + "jours");
        //System.out.printf("%20s %2d jours ", "reprise an dans:", stageJoursNb);
        //System.out.println("");

        //System.out.println("entrée en stage dans:" + dateStageDebutDiff.getMonths() + "mois " + dateStageDebutDiff.getDays() + "jours");
        System.out.printf("%25s:%2d mois %2d jours ", "Entrée en stage dans", dateStageDebutDiff.getMonths(),  dateStageDebutDiff.getDays());
        System.out.println("");

        //System.out.println("durée du stage:"       + period_stageDuree.getMonths()  + "mois " + period_stageDuree.getDays() + "jours");
        System.out.printf("%25s:%2d mois %2d jours ", "Durée du stage", 
                period_stageDuree.getMonths(), period_stageDuree.getDays());
        System.out.println("");

        //System.out.println("sortie de stage dans:" + dateStageFinDiff.getMonths()   + "mois " + dateStageFinDiff.getDays()   + "jours");
        System.out.printf("%25s:%2d mois %2d jours ", "Sortie de stage dans",
                dateStageFinDiff.getMonths(), dateStageFinDiff.getDays());
        System.out.println("");

        //System.out.println("examen dans:"          + dateExamenDiff.getMonths()     + "mois " + dateExamenDiff.getDays() + "jours");
        System.out.printf("%25s:%2d mois %2d jours ", "Examen dans",
                dateExamenDiff.getMonths(), dateExamenDiff.getDays());
        System.out.println("");

        //System.out.println("Temps restant(sans le stage):" + dateFormationRestant.getMonths() + "mois " + dateFormationRestant.getDays() + "jours");
        System.out.printf("%25s:%2d mois %2d jours ", "Temps restant(sans stage)",
                dateFormationRestant.getMonths(), dateFormationRestant.getDays());
        System.out.println("");
    }
}
